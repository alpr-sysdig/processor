(* Bibliothèque pour produire du code RISC-V

   2018 ALPR Team
*)

open Format

type register =  string
type immediate = string


type asm =
  | Nop
  | S of string
  | Cat of asm * asm
type program = asm

let nop = Nop
let inline s = S s
let (++) x y = Cat (x, y)

let buf = Buffer.create 17
let fmt = formatter_of_buffer buf
let ins x =
  (* Buffer.add_char buf '\t'; *)
  kfprintf (fun fmt ->
    fprintf fmt "\n";
    pp_print_flush fmt ();
    let s = Buffer.contents buf in
    Buffer.clear buf;
    S s
  ) fmt x

let rec pr_asm fmt = function
  | Nop          -> ()
  | S s          -> fprintf fmt "%s" s
  | Cat (a1, a2) -> pr_asm fmt a1; pr_asm fmt a2

let print_program fmt p =
  pr_asm fmt p;
  pp_print_flush fmt ()

let print_in_file ~file p =
  let c = open_out file in
  let fmt = formatter_of_out_channel c in
  print_program fmt p;
  close_out c


let x0  = "00000"
let x1  = "00001"
let x2  = "00010"
let x3  = "00011"
let x4  = "00100"
let x5  = "00101"
let x6  = "00110"
let x7  = "00111"
let x8  = "01000"
let x9  = "01001"
let x10 = "01010"
let x11 = "01011"
let x12 = "01100"
let x13 = "01101"
let x14 = "01110"
let x15 = "01111"
let x16 = "10000"
let x17 = "10001"
let x18 = "10010"
let x19 = "10011"
let x20 = "10100"
let x21 = "10101"
let x22 = "10110"
let x23 = "10111"
let x24 = "11000"
let x25 = "11001"
let x26 = "11010"
let x27 = "11011"
let x28 = "11100"
let x29 = "11101"
let x30 = "11110"
let x31 = "11111"




(* assumes a non-neg nb *)
let binstring_of_int n =
  let rec aux acc = function
    | 0 -> (*I belive the asm uses the other convention and it's rev in decode -> to check *) List.rev acc
    | d -> aux (string_of_int (d land 1)::acc) (d lsr 1)
  in
  let s = String.concat "" (aux [] n) in s

let extend n extension s  = match String.length s with
  |l when l < n -> s ^ (String.make (n - l) extension)
  |l when l > n -> String.sub s 0 n
  |_ -> s


let reg r = fun fmt () -> fprintf fmt "%s" r
let imm = function
  |i when i < 0 -> (1 lsl 32) + i |> binstring_of_int |> extend 32 '1'
  |i -> i |> binstring_of_int |> extend 32 '0'

let shamt i = i |> binstring_of_int |> extend 5 '0'

let rev_sub str start len =
  String.init len (function i -> str.[start + len - i - 1])

(* we assume imm is in most significant digit first *)
let r_type opcode funct3 funct7 rd rs1 rs2 = ins "%s%a%s%a%a%s" opcode rd () funct3 rs1 () rs2 () funct7
let i_type opcode funct3 rd rs1 i =
  let i = rev_sub i 0 12 in
  ins "%s%a%s%a%s" opcode rd () funct3 rs1 () i
let b_type opcode funct3 rs1 rs2 i =
  let i = String.sub i 0 13 in  (* index 0 is not used *)
  ins "%s%s%s%a%a%s" opcode ((String.make 1 (i.[11])) ^ (rev_sub i 1 4)) funct3 rs1 () rs2 () ((rev_sub i 5 6) ^ (String.make 1 (i.[12])))
let s_type opcode funct3 rs1 rs2 i =
  let i = String.sub i 0 12 in
  (* not sure of the rev_sub here but sure for b_type *)
  ins "%s%s%s%a%a%s" opcode (rev_sub i 0 5) funct3 rs1 () rs2 () (rev_sub i 5 7)
let si_type opcode funct3 funct7  rd rs1 sht =
  let sht = shamt sht in
  ins "%s%a%s%a%s%s" opcode rd () funct3 rs1 () sht funct7

let rec fill = function
  |0 -> failwith "error"
  |1 -> ins "00000000000000000000000000000000"
  |n -> (fill 1) ++ (fill (n - 1))


let lw = i_type "1100000" "010"
let sw = s_type "1100010" "010"

let add  = r_type "1100110" "000" "0000000"
let addi = i_type "1100100" "000"
let sub  = r_type "1100110" "000" "0000010"

let sll  = r_type "1100110" "100" "0000000"
let srl  = r_type "1100110" "101" "0000000"
let sra  = r_type "1100110" "101" "0000010"
let slli =si_type "1100100" "100" "0000000"
let srli =si_type "1100100" "101" "0000000"
let srai =si_type "1100100" "101" "0000010"

let xor   =r_type "1100110" "001" "0000000"
let _or   =r_type "1100110" "011" "0000000"
let _and  =r_type "1100110" "111" "0000000"
let xori  =  i_type "1100100" "001"
let _ori  =  i_type "1100100" "011"
let _andi =  i_type "1100100" "111"

let jalr = i_type  "1110011" "000"

let beq =  b_type "1100011" "000"
let bne =  b_type "1100011" "100"
let blt =  b_type "1100011" "001"
let bge =  b_type "1100011" "101"
let bltu = b_type "1100011" "011"
let bgeu = b_type "1100011" "111"

let mul = r_type "1100110" "000" "1000000"
let div = r_type "1100110" "001" "1000000"
let rem = r_type "1100110" "011" "1000000"

let slt  = r_type "1100110" "010" "0000000"
let sltu = r_type "1100110" "110" "0000000"
let slti = i_type "1100100" "010"
let sltiu =i_type "1100100" "110"


(* this doesn't do anything, time is hardwired to x1 *)
let rdtime rd = i_type "1100111" "010" (reg x0) (reg x0) (imm 0)


