
let jump1 = 4;

addi   (reg x2) (reg x0) (imm 1)

addi   (reg x1) (reg x0) (imm 0)        ++
sw     (reg x2) (reg x0) (imm 0)        ++
addi   (reg x1) (reg x0) (imm 31)       ++
sw     (reg x2) (reg x1) (imm 1)        ++ (* January *)
addi   (reg x1) (reg x1) (imm 28)       ++
sw     (reg x2) (reg x1) (imm 2)        ++ (* February *)
addi   (reg x1) (reg x1) (imm 31)       ++
sw     (reg x2) (reg x1) (imm 3)        ++ (* March *)
addi   (reg x1) (reg x1) (imm 30)       ++
sw     (reg x2) (reg x1) (imm 4)        ++ (* April *)
addi   (reg x1) (reg x1) (imm 31)       ++
sw     (reg x2) (reg x1) (imm 5)        ++ (* May *)
addi   (reg x1) (reg x1) (imm 30)       ++
sw     (reg x2) (reg x1) (imm 6)        ++ (* June *)
addi   (reg x1) (reg x1) (imm 31)       ++
sw     (reg x2) (reg x1) (imm 7)        ++ (* July *)
addi   (reg x1) (reg x1) (imm 31)       ++
sw     (reg x2) (reg x1) (imm 8)        ++ (* August *)
addi   (reg x1) (reg x1) (imm 30)       ++
sw     (reg x2) (reg x1) (imm 9)        ++ (* September *)
addi   (reg x1) (reg x1) (imm 31)       ++
sw     (reg x2) (reg x1) (imm 10)       ++ (* October *)
addi   (reg x1) (reg x1) (imm 30)       ++
sw     (reg x2) (reg x1) (imm 11)       ++ (* November *)
addi   (reg x1) (reg x1) (imm 31)       ++
sw     (reg x2) (reg x1) (imm 12)       ++ (* December *)

addi   (reg x2) (reg x0) (imm 14)

addi   (reg x1) (reg x0) (imm 0)        ++
sw     (reg x2) (reg x0) (imm 0)        ++
addi   (reg x1) (reg x0) (imm 31)       ++
sw     (reg x0) (reg x1) (imm 1)        ++ (* January *)
addi   (reg x1) (reg x1) (imm 28)       ++
sw     (reg x0) (reg x1) (imm 2)        ++ (* February *)
addi   (reg x1) (reg x1) (imm 31)       ++
sw     (reg x0) (reg x1) (imm 3)        ++ (* March *)
addi   (reg x1) (reg x1) (imm 30)       ++
sw     (reg x0) (reg x1) (imm 4)        ++ (* April *)
addi   (reg x1) (reg x1) (imm 31)       ++
sw     (reg x0) (reg x1) (imm 5)        ++ (* May *)
addi   (reg x1) (reg x1) (imm 30)       ++
sw     (reg x0) (reg x1) (imm 6)        ++ (* June *)
addi   (reg x1) (reg x1) (imm 31)       ++
sw     (reg x0) (reg x1) (imm 7)        ++ (* July *)
addi   (reg x1) (reg x1) (imm 31)       ++
sw     (reg x0) (reg x1) (imm 8)        ++ (* August *)
addi   (reg x1) (reg x1) (imm 30)       ++
sw     (reg x0) (reg x1) (imm 9)        ++ (* September *)
addi   (reg x1) (reg x1) (imm 31)       ++
sw     (reg x0) (reg x1) (imm 10)       ++ (* October *)
addi   (reg x1) (reg x1) (imm 30)       ++
sw     (reg x0) (reg x1) (imm 11)       ++ (* November *)
addi   (reg x1) (reg x1) (imm 31)       ++
sw     (reg x0) (reg x1) (imm 12)       ++ (* December *)

rdtime (reg x1)                         ++ (* get the number of milisecond from 00:00 am, 1st January 2017 *)
addi   (reg x2) (reg x0) (imm 1000)     ++ (* x2 = 1000 *)
addi   (reg x3) (reg x0) (imm 24)       ++ (* x3 = 24 hour *)
addi   (reg x4) (reg x0) (imm 60)       ++ (* x4 = 60 second/minute *)
addi   (reg x5) (reg x0) (imm 2017)     ++ (* the starting year *)
addi   (reg x6) (reg x0) (imm 365)      ++ (* x6 = 365 the number of days in one year *)
addi   (reg x7) (reg x0) (imm 1461)     ++ (* x7 = 1461 = 365 * 3 + 366 the number of days in 4 consecutive years *)
addi   (reg x8) (reg x0) (imm 1095)     ++ (* x8 = 365 µ 3 the number of days in the first 3 years*)

div    (reg x15) (reg x1) (reg x2)      ++ (* x15 = the number of SECONDS passed by *)
div    (reg x16) (reg x15) (reg x4)     ++ (* x16 = the number of MINUTES passed by *)
div    (reg x17) (reg x16) (reg x4)     ++ (* x17 = the number of HOURS passed by *)
div    (reg x18) (reg x17) (reg x3)     ++ (* x18 = the number of DAYS passed by *)

(* Done with SECONDS, MINUTES, HOURS, could free x15, x16, x17 *)

rem    (reg x26) (reg x15) (reg x4)     ++ (* x26 = the current SECOND *)
rem    (reg x27) (reg x16) (reg x4)     ++ (* x27 = the current MINUTE *)
rem    (reg x28) (reg x17) (reg x3)     ++ (* x28 = the current HOUR *)

(* Calculate DAYS, MONTHS, YEARS *)

div    (reg x19) (reg x18) (reg x7)     ++ (* x19 = the number of 4 YEARS passed by *)
rem    (reg x20) (reg x18) (reg x7)     ++ (* x20 = the number of days still left *)
blt    (reg x21) (reg x8) (imm 18*4)      ++ (* conditional TODO:: How many jump *)

(* At this branch, each Feb has 28 days *)

addi   (reg x11) (reg x0) (imm 4)       ++
mul    (reg x11) (reg x11) (reg x19)    ++
div    (reg x12) (reg x20) (reg x6)     ++ (* x12 = the number of years in 4 years*)
rem    (reg x15) (reg x20) (reg x6)     ++ (* x15 = the number of day lefts in one year*)
add    (reg x11) (reg x11) (reg x12)    ++
add    (reg x31) (reg x11) (reg x5)     ++ (* x31 = the current YEAR *)
addi   (reg x10) (reg x0) (imm 1)       ++ (* x10 = the starting point *)
addi   (reg x11) (reg x0) (imm 1)       ++ (* Init the loop to check*)
addi   (reg x11) (reg x11) (imm 1)      ++
lw     (reg x12) (reg x11) (imm 0)      ++
blt    (reg x15) (reg x12) (imm -12)    ++ (* If true, jump to addi (reg x11), TODO::dont know is it -8 or -12 *)
sub    (reg x30) (reg x11) (reg x10)    ++ (* If fail, x30 = x11 - x10 = the current MONTH *)
addi   (reg x11) (reg x11) (imm -1)     ++
lw     (reg x12) (reg x11) (imm 0)      ++ (* x12 contains the total days from previous months *)
sub    (reg x29) (reg x15) (reg x12)    ++
addi   (reg x29) (reg x29) (imm 1)      ++ (* x29 = the current DAY *)
jalr   (reg x9) (reg x0) (imm -35*4)    ++ (* jump back to RDTIME *)

(* At this branch, Feb has 29 days *)

addi   (reg x11) (reg x0) (imm 4)       ++
mul    (reg x11) (reg x11) (reg x19)    ++
addi   (reg x11) (reg x11) (imm 3)      ++
add    (reg x31) (reg x11) (reg x5)     ++ (* x31 = the current YEAR *)
addi   (reg x10) (reg x0) (imm 14)       ++ (* x10 = the starting point *)
addi   (reg x11) (reg x0) (imm 14)       ++ (* Init the loop to check*)
addi   (reg x11) (reg x11) (imm 1)      ++
lw     (reg x12) (reg x11) (imm 0)      ++
blt    (reg x15) (reg x12) (imm -12)    ++ (* If true, jump to addi (reg x11), TODO::dont know is it -8 or -12 *)
sub    (reg x30) (reg x11) (reg x10)    ++ (* If fail, x30 = x11 - x10 = the current MONTH *)
addi   (reg x11) (reg x11) (imm -1)     ++
lw     (reg x12) (reg x11) (imm 0)      ++ (* x12 contains the total days from previous months *)
sub    (reg x29) (reg x15) (reg x12)    ++
addi   (reg x29) (reg x29) (imm 1)      ++ (* x29 = the current DAY *)
jalr   (reg x9) (reg x0) (imm -35*4)    ++ (* jump back to RDTIME *)

(* In the end x26 - x31 contains SECOND, MINUTE, HOUR, DAY, MONTH, YEAR in the same order *)
