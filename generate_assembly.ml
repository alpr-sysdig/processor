open RISCV

let main =
  let ofile = "/home/waissfowl/Documents/Cours-systeme-digital/processor/instr.rom" in
  let oc = open_out ofile in
  let code =
    addi (reg x1) (reg x0) (imm 1) ++
    addi (reg x2) (reg x0) (imm 2) ++
    beq (reg x1) (reg x2) (imm 2) ++
      (* you have to put 16 instructions in the instr rom so you can use this function to fill the remaining space *)
    fill (16 - 3)















  in
  RISCV.print_program (Format.formatter_of_out_channel oc) code;
