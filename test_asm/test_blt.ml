open RISCV

let main =
  let ofile = "/home/waissfowl/Documents/Cours-systeme-digital/processor/instr.rom" in
  let oc = open_out ofile in
  let nb_instr = 2 in
  let code =

    (*Test code*)
    addi (reg x1) (reg x0) (imm (-7)) ++
    addi (reg x2) (reg x0) (imm (5)) ++
    blt (reg x1) (reg x2) (imm (- 4 * nb_instr)) ++


(* you have to put 16 instructions in the instr rom so you can use this function to fill the remaining space *)
    fill (16 - nb_instr)















  in
  RISCV.print_program (Format.formatter_of_out_channel oc) code;
