open RISCV

let main =
  let ofile = "/home/waissfowl/Documents/Cours-systeme-digital/processor/instr.rom" in
  let oc = open_out ofile in
  let nb_instr = 4 in
  let code =

    (*Test code*)
    addi (reg x1) (reg x0) (imm 15) ++
    addi (reg x2) (reg x0) (imm 2) ++
    div  (reg x1) (reg x1) (reg x2) ++
    addi (reg x2) (reg x0) (imm 7) ++

    (*Verification : we get back to 0 if it's ok*)    
    beq (reg x1) (reg x2) (imm (-4 *nb_instr)) ++
      (* you have to put 16 instructions in the instr rom so you can use this function to fill the remaining space *)
    fill (16 - nb_instr - 1)















  in
  RISCV.print_program (Format.formatter_of_out_channel oc) code;
