open RISCV

let main =
  let ofile = "/home/waissfowl/Documents/Cours-systeme-digital/processor/instr.rom" in
  let oc = open_out ofile in
  let nb_instr = 1 in
  let code =

    (*Test code*)
    addi (reg x2) (reg x1) (imm 0) ++



      (* you have to put 16 instructions in the instr rom so you can use this function to fill the remaining space *)
    fill (16 - nb_instr)















  in
  RISCV.print_program (Format.formatter_of_out_channel oc) code;
