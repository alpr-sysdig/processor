#!/usr/bin/env ruby

$run_script = "../netlist-compiler/run"
$randomized_count = 10000
$skip_cleanup = true

require "test/unit"
require "open3"

def rand32
  (rand() * 2 ** 32 - 2 ** 31).to_i
end

class Integer
  def to_b
    n = (self + 2 ** 31) % (2 ** 32) - 2 ** 31
    n = n < 0 ? 2 ** 32 + n : n
    b = [n % 2]
    b.unshift n % 2 while (n >>= 1) > 0
    b.join.rjust(32, '0').force_encoding("utf-8")
  end
end

class TestSimulator < Test::Unit::TestCase
  def capture_output(command, input=nil)
    output = ""
    Open3.popen3(command) do |stdin, stdout, stderr|
      input.each {|c| stdin.puts c } unless input.nil?
      output = stdout.read.chomp
    end
    res = {}
    output.split("\n").each do |line|
      m = /\s*(\w+)\s*[:=]\s*([01]+)\s*/.match(line)
      res[m[1]] = m[2] if m
    end
    res["res"]
  end

  def prgrm(str, inc=[])
    "#{inc.map { |i| "#include \"#{i}\""}.join("\n")}
    main(arg1:[32], arg2:[32]) = res:[32] where
      reg_data1 = ram<2, 2>(0b00, 0, 0b00,  0b00);
      reg_data2 = ram<2, 2>(0b00, 0, 0b00,  0b00);
      res = #{str}
    end where" % ["arg1", "arg2"]
  end

  def build_prgrm prgrm
    File.open("auto_test.mj", 'w') do |f|
      f.puts prgrm
    end
    system "#{$run_script} auto_test.mj -o auto_test"
  ensure
    system "rm -f auto_test.mj" unless $skip_cleanup
  end

  def error_msg(x, y, z, r, op:"operation", sym:" ")
    "\n\e[0;33m" + " Failed on #{op} ".center(37,"-") + "\e[0m\n" +
      " " * 5 + x + "\n" +
      sym.center(5,' ') + y + "\n" +
      "=".center(5,' ') + "\e[0;32m" + z + "\n" + "\e[0m" +
      "->".center(5,' ') + "\e[0;31m" + r + "\n" +
      "\e[0;33m" + "-" * 37 + "\e[0m\n"
  end

  def multiple_test(instr, f, op, sym, testcases = nil)
    build_prgrm (prgrm instr, ["alu.mj"])
    testcases ||= (0...$randomized_count).map { [rand32(), rand32()] }
    testcases.each do |a, b|
      r = capture_output "./auto_test", [a.to_b, b.to_b]
      assert_equal (f.call(a,b)).to_b, r, error_msg(a.to_b, b.to_b, (f.call(a,b)).to_b, r, op:op, sym:sym)
    end
  ensure
    system "rm -f ./auto_test" unless $skip_cleanup
  end

  # Test are evaluated in lexicographical order and must start with "test_" prefix
  def test_aaa_bitconversion
    assert_equal("0" * 32, 0.to_b)
    assert_equal( ("0" * 28) + "1000", 8.to_b)
    assert_equal(4.to_b, (2 * 2).to_b)
    assert_equal("0" + "1" * 31, (2 ** 31 - 1).to_b)
    assert_equal("1" + "0" * 31, (- 2 ** 31).to_b)
    assert_equal((- 2 ** 31).to_b, (2 ** 31).to_b)
  end

  def test_add
    multiple_test("ADD(%s, %s)", ->(a,b) { a + b }, "addition", "+")
  end

  def test_sub
    multiple_test("SUB(%s, %s)", ->(a,b) { a - b }, "substraction", "-")
  end

  def test_mul
    multiple_test("MUL(%s, %s)", ->(a,b) { a * b }, "multiplication", "*")
  end

  def test_sltu
    multiple_test("SLT(%s, %s)", ->(a,b) { if a < b then 2**31 else 0 end}, "slt", "<")
  end

  def test_quot
    multiple_test("QUOT(%s, %s)", ->(a,b) { a / b }, "div", "/",
                 [[4,2], [8,4], [9,3], [10,3], [2, 100],
                  [129, 64], [1231324,48317], [4319746,42932],
                  [8, -2], [4, -1], [-4, 2], [-128, 64], [-129, 64]])
    multiple_test("QUOT(%s, %s)", ->(a,b) { a / b }, "div", "/")
  end

  def test_rem
    multiple_test("REM(%s, %s)", ->(a,b) { a % b }, "rem", "/")
  end
end
